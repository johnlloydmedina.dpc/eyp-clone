// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
document.addEventListener('DOMContentLoaded', function () {
    const searchInput = document.getElementById('search-input');

    const tabs = {
        'tab-all': 'Search for businesses, products, or services',
        'tab-business': 'Search for businesses',
        'tab-products': 'Search for products',
        'tab-services': 'Search for services',
        'tab-phone': 'Search for phone numbers'
    };

    Object.keys(tabs).forEach(tabId => {
        document.getElementById(tabId).addEventListener('click', function () {
            searchInput.placeholder = tabs[tabId];
        });
    });
});

